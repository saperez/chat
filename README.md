                                        PROGRAMACIÓN DE SERVICIOS Y PROCESOS
                                    UT03. Programación de Comunicaciones en Red
                                                Práctica a entregar

Chat cliente-servidor


Situación típica de un servidor que atiende a múltiples clientes es un servidor de chat. Chat sencillo que puede atender a varios clientes a la vez, cada cliente será 
atendido en un hilo de ejecución; en ese hilo se recibirán sus mensajes y se enviarán al resto.

La aplicación se compone de tres clases:

ClienteChat.java:

Crea un socket para conectarse al servidor por medio del puerto indicado.


ServidorChat.java:

Está en modo escucha a la espera de cualquier petición de conexión de los clientes. Admite un máximo de 10 conexiones y si se cierra se desconectan todos los clientes.


HiloServidor.java:

Se encarga de recibir y enviar los mensajes a los clientes del Chat. 



